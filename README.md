## Laravel Generantor

IHero Laravel Generantor Features

- Generantor Commands
    - Controller
    - Repository
    - Criteria
    - Service
    - Model
    - Views

### 安裝方法

#### 在新 Laravel 專案中的執行以下指令

** 加入安裝來源
```
composer config repositories.ihero-generator vcs https://gitlab.avp/ihero/laravel-generantor.git
```

** 安裝套件
```
composer require ihero/generator
```

** 加入指令 (Laravel 5 以上不需執行)

在 Laravel 專案 config/app.php 中，將 `IHero\Generantor\Providers\IheroGeneratorServiceProvider::class` 加到 `providers` 陣列變數中

```
'providers' => [
    ...
    IHero\Generantor\Providers\IheroGeneratorServiceProvider::class,
],
```


### 指令

```
php artisan ih.make:controller {$NAME} --serv={$SERVICE_NAME}     Controller generator command
php artisan ih.make:service {$NAME} --repo={$REPOSITORY_NAME}     Service generator command
php artisan ih.make:repository {$NAME} --model={$Model_NAME}    Repository generator command
php artisan ih.make:views {$NAME}                                 View generator command
php artisan ih.make:criteria {$NAME}                              Criteria generator command
```


### 常用套件

#### 權限

```
composer require spatie/laravel-permission
```

#### 語系轉換

```
composer require spatie/laravel-translatable
```

#### Log 記錄

```
composer require spatie/laravel-activitylog
```

#### AdminLTE for laravel

(Doc)[https://github.com/jeroennoten/Laravel-AdminLTE#610-menu]

```
composer require jeroennoten/laravel-adminlte
```
