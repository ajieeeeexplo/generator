<?php

namespace Ihero\Generator\Repositories\Contracts;

interface RepositoryInterface
{

    public function all($columns = ['*']);

    public function paginate($perPage = 15, $columns = ['*']);

    public function create(array $data);

    public function update($id, array $data);

    public function delete($id);

    public function find($id, $columns = ['*']);

}
