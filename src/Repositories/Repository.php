<?php

namespace Ihero\Generator\Repositories;

use Str;
use Ihero\Generator\Repositories\Contracts\CriteriaInterface;
use Ihero\Generator\Repositories\Contracts\RepositoryInterface;
use Ihero\Generator\Repositories\Criteria\Criteria;
use Illuminate\Support\Collection;

abstract class Repository implements RepositoryInterface, CriteriaInterface
{
    /**
     * This repositories the model.
     *
     * @var Illuminate\Database\Eloquent\Model
     */
    protected $model;

    /**
     * @var Collection
     */
    protected $criteria;

    /**
     * @var bool
     */
    protected $skipCriteria = false;

    /**
     * Undocumented function
     *
     * @return Illuminate\Database\Eloquent\Model
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Retrieve all records with given filter criteria
     *
     * @param  array $columns
     * @return \Illuminate\Database\Eloquent\Collection|Model|null
     */
    public function all($columns = ['*'])
    {
        $this->applyCriteria();

        return $this->model->get($columns);
    }

    /**
     * Find model record for given id
     *
     * @param int $id
     * @param array $columns
     *
     * @return \Illuminate\Database\Eloquent\Collection|Model|null
     */
    public function find($id, $columns = ['*'])
    {
        $this->applyCriteria();

        return $this->model->find($id, $columns);
    }

    /**
     * Get the model by id.
     *
     * @param  int|string $id
     * @return \Illuminate\Database\Eloquent\Collection|Model|null
     */
    public function getTrashedById($id)
    {
        return $this->model->onlyTrashed()->find($id);
    }

    /**
     * Create a row record.
     *
     * @param  array $data
     * @return Illuminate\Database\Eloquent\Model
     */
    public function create(array $data)
    {
        $model = $this->model->newInstance($data);
        $model->save();

        return $model;
    }

    /**
     * Update the row record by primary key id.
     *
     * @param  string|int $id
     * @param  array  $data
     * @return Illuminate\Database\Eloquent\Model
     */
    public function update($id, array $data)
    {
        $model = $this->model->findOrFail($id);
        $model->fill($data);
        $model->save();

        return $model;
    }

    /**
     * Update the data row by model.
     *
     * @param  Illuminate\Database\Eloquent\Model $model
     * @param  array $data
     * @return Illuminate\Database\Eloquent\Model
     */
    public function updateByModel($model, array $data)
    {
        foreach ($this->model->getAttributes() as $key => $value) {
            if (!array_key_exists($key, $data)) {
                $data[$key] = $value;
            }
        }
        $model->update($data);

        return $model;
    }

    /**
     * Delete the row record by primary key id.
     *
     * @param  int|string $id
     * @return bool
     */
    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    /**
     * Delete the row by model.
     *
     * @param  Illuminate\Database\Eloquent\Model $model
     * @return bool
     */
    public function deleteByModel($model)
    {
        return $model->delete();
    }

    /**
     * When need paginate fileted data, can use this method.
     *
     * @param int $perPage
     * @param array $columns
     * @return void
     */
    public function paginate($perPage = 15, $columns = ['*'])
    {
        $this->applyCriteria();

        return $this->model->paginate($perPage, $columns);
    }

    /**
     * Sort the row record.
     *
     * @param string $column
     * @param string $sort
     * @return $this
     */
    public function sortBy(string $column, string $sort = 'asc')
    {
        $this->model->orderby($column, $sort);

        return $this;
    }

    /**
     * @return $this
     */
    public function resetScope()
    {
        $this->skipCriteria(false);

        return $this;
    }

    /**
     * @param bool $status
     *
     * @return $this
     */
    public function skipCriteria($status = true)
    {
        $this->skipCriteria = $status;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCriteria()
    {
        return $this->criteria ?? collect([]);
    }

    /**
     * @param Criteria $criteria
     *
     * @return $this
     */
    public function getByCriteria(Criteria $criteria)
    {
        $this->model = $criteria->apply($this->model, $this);

        return $this;
    }

    /**
     * @param Criteria $criteria
     *
     * @return $this
     */
    public function pushCriteria(Criteria $criteria)
    {
        $this->criteria->push($criteria);

        return $this;
    }

    /**
     * @return $this
     */
    public function applyCriteria()
    {
        if ($this->skipCriteria === true) {
            return $this;
        }

        foreach ($this->getCriteria() as $criteria) {
            if ($criteria instanceof Criteria) {
                $this->model = $criteria->apply($this->model, $this);
            }
        }

        return $this;
    }

    /**
     * Call the Repository magic method.
     *
     * @param [string] $method
     * @param [array] $args
     * @return void
     */
    public function __call(string $method, array $args)
    {
        if (Str::is('findBy*', $method)) {
            if (count($args) == 1) {
                return $this->findBy(substr($method, 6), $args[0]);
            }
        }
    }

    /**
     * Auto make Get Function by condition.
     *
     * @param string $column
     * @param string|int|null $value
     * @return void
     */
    public function findBy(string $column, $value = null)
    {
        if (filled($value)) {
            $column = Str::studly($column);
            $column = Str::snake($column);

            $model = $this->findByCondition($column, $value);

            return $model->get();
        }
        return collect([]);
    }

    /**
     * Get the model data by condition.
     *
     * @param string $column
     * @param string|int $arg
     * @return void
     */
    protected function findByCondition(string $column, $value)
    {
        $this->applyCriteria();

        return $this->model->where($column, $value);
    }
}
