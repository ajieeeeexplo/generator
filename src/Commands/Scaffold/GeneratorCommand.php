<?php

namespace Ihero\Generator\Commands\Scaffold;

use Str;
use Exception;
use Illuminate\Console\Command;
use Ihero\Generator\Exceptions\RuntimeException;

class GeneratorCommand extends Command
{
    /**
     * Get template content.
     *
     * @return Mixed
     */
    protected function getTemplateContent()
    {
        if (file_exists($this->templateSource)) {
            return file_get_contents($this->templateSource);
        }

        throw new RuntimeException('Unable to get template content.');
    }

    /**
     * Get content after overwrited tags.
     *
     * @param String $content
     * @param Array $tags
     * @return String
     */
    protected function getOverwritedContentWithTags(String $content, Array $tags)
    {
        foreach ($tags as $tag => $value) {
            $content = str_replace(sprintf('!!%s!!', Str::studly($tag)), Str::studly($value), $content);
            $content = str_replace(sprintf('!!%s!!', Str::camel($tag)), Str::camel($value), $content);
        }

        return $content;
    }

    /**
     * Generate storing directory and path
     *
     * @param String $filename
     * @return String
     */
    protected function generateStoringDirAndPath(String $filename)
    {
        if (!is_dir(base_path($this->storingPath))) {
            mkdir(base_path($this->storingPath));
        }

        return sprintf('%s/%s', base_path($this->storingPath), $filename);
    }

    /**
     * Generate file and write the content.
     *
     * @param String $filePath
     * @param String $content
     */
    protected function generateFileAndWriteContent(String $filePath, String $content)
    {
        if (file_exists($filePath) && !$this->confirm('This file already exists. Do you wish to continue?')) {
            throw new RuntimeException('Unable to generate file.');
        }

        if ($file = fopen($filePath, 'w')) {
            fwrite($file, $content);
            fclose($file);
        } else {
            throw new RuntimeException('Unable to open file.');
        }
    }
}
