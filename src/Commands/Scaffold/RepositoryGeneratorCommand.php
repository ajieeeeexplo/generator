<?php

namespace Ihero\Generator\Commands\Scaffold;

use Str;
use Illuminate\Console\GeneratorCommand;
use Ihero\Generator\Exceptions\RuntimeException;

class RepositoryGeneratorCommand extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ih.make:repository
                                {name : The name of the class.}
                                {--model= : The name of model.}
                                {--empty : Blank version of repository.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new Repository class';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Repository';

    /**
     * Get the stub file for the generator.
     *
     * @param  string  $type
     * @return string
     */
    protected function getStub(string $type = '')
    {
        if ($type == 'model') {
            return __DIR__.'/../../../stubs/repository.model.stub';
        }
        return __DIR__.'/../../../stubs/repository.stub';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\Repositories';
    }


    /**
     * Build the class with the given name.
     *
     * @param  string  $name
     * @return string
     */
    protected function buildClass($name)
    {
        if ($this->option('empty')) {
            $stub = $this->files->get($this->getStub());

            return $this
                ->replaceNamespace($stub, $name)
                ->replaceClass($stub, $name);
        } else {
            $stub = $this->files->get($this->getStub('model'));
            $replace = $this->buildModelReplacements();

            return $this
                ->replaceNamespace($stub, $name)
                ->replaceModelClass($stub, $replace)
                ->replaceClass($stub, $name);
        }
    }

    /**
     * Build the model replacement values.
     *
     * @return array
     */
    protected function buildModelReplacements()
    {
        $modelClass = $this->parseModel($this->option('model') ?? $this->getNameInput());

        if (! class_exists($modelClass)) {
            if ($this->confirm("A {$modelClass} model does not exist. Do you want to generate it?", true)) {
                $this->call('ih.make:model', ['name' => $modelClass]);
            }
        }

        return [
            'DummyFullModelClass' => $modelClass
        ];
    }

    /**
     * Get the desired class name from the input.
     *
     * @return string
     */
    protected function getNameInput()
    {
        $name = trim($this->argument('name'));
        $name = Str::studly($name);

        return Str::is('*Repository', $name) ?
            $name : Str::finish($name, 'Repository');
    }

    /**
     * Get the fully-qualified model class name.
     *
     * @param  string  $model
     * @return string
     *
     * @throws RuntimeException
     */
    protected function parseModel($model)
    {
        if (preg_match('([^A-Za-z0-9_/\\\\])', $model)) {
            throw new RuntimeException('Model name contains invalid characters.');
        }

        $model = trim(str_replace('/', '\\', $model), '\\');
        $model = str_replace('Repository', '', $model);

        if (Str::startsWith($model, $rootNamespace = $this->laravel->getNamespace())) {
            $model = class_basename($model);
        }

        return sprintf('%sModels\\%s', $rootNamespace, $model);
    }

    /**
     * Replace the model class for the given stub.
     *
     * @param  string  $stub
     * @param  array  $replace
     * @return $this
     */
    protected function replaceModelClass(string &$stub, array $replace)
    {
        $stub = str_replace(
            array_keys($replace),
            array_values($replace),
            $stub
        );

        return $this;
    }
}
