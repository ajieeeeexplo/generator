<?php

namespace Ihero\Generator\Commands\Scaffold;

use Str;
use Illuminate\Console\GeneratorCommand;
use Ihero\Generator\Exceptions\RuntimeException;

class ControllerGeneratorCommand extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ih.make:controller
                                {name : The name of the class.}
                                {--serv= : The name of service.}
                                {--empty : Blank version of controller.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new Controller class';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Controller';

    /**
     * Get the stub file for the generator.
     *
     * @param  string  $type
     * @return string
     */
    protected function getStub(string $type = '')
    {
        if ($type == 'serv') {
            return __DIR__.'/../../../stubs/controller.serv.stub';
        }
        return __DIR__.'/../../../stubs/controller.stub';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\Http\Controllers';
    }

    /**
     * Build the class with the given name.
     *
     * @param  string  $name
     * @return string
     */
    protected function buildClass($name)
    {
        if ($this->option('empty')) {
            $stub = $this->files->get($this->getStub());

            return $this
                ->replaceNamespace($stub, $name)
                ->replaceClass($stub, $name);
        } else {
            $stub = $this->files->get($this->getStub('serv'));
            $replace = $this->buildServReplacements();

            return $this
                ->replaceNamespace($stub, $name)
                ->replaceServClass($stub, $replace)
                ->replaceClass($stub, $name);
        }
    }

    /**
     * Build the model replacement values.
     *
     * @return array
     */
    protected function buildServReplacements()
    {
        $servClass = $this->parseServ($this->option('serv') ?? $this->getNameInput());

        if (! class_exists($servClass)) {
            if ($this->confirm("A {$servClass} service does not exist. Do you want to generate it?", true)) {
                $this->call('ih.make:service', ['name' => $servClass]);
            }
        }

        return [
            'DummyFullServiceClass' => $servClass,
            'DummyServiceClass' => class_basename($servClass),
            'DummyServiceVariable' => lcfirst(class_basename($servClass)),
            'DummyTebleName' => strtolower(str_replace('Service', '', class_basename($servClass)))
        ];
    }

    /**
     * Get the desired class name from the input.
     *
     * @return string
     */
    protected function getNameInput()
    {
        $name = trim($this->argument('name'));
        $name = Str::studly($name);

        return Str::is('*Controller', $name) ?
            $name : Str::finish($name, 'Controller');
    }

    /**
     * Get the fully-qualified model class name.
     *
     * @param  string  $model
     * @return string
     *
     * @throws RuntimeException
     */
    protected function parseServ($service)
    {
        if (preg_match('([^A-Za-z0-9_/\\\\])', $service)) {
            throw new RuntimeException('Service name contains invalid characters.');
        }

        $service = trim(str_replace('/', '\\', $service), '\\');
        $service = str_replace('Controller', '', $service);

        if (! Str::is('*Service', $service)) {
            $service = Str::finish($service, 'Service');
        }
        if (Str::startsWith($service, $rootNamespace = $this->laravel->getNamespace())) {
            $service = class_basename($service);
        }

        return sprintf('%sServices\\%s', $rootNamespace, $service);
    }

    /**
     * Replace the model class for the given stub.
     *
     * @param  string  $stub
     * @param  array  $replace
     * @return $this
     */
    protected function replaceServClass(string &$stub, array $replace)
    {
        $stub = str_replace(
            array_keys($replace),
            array_values($replace),
            $stub
        );

        return $this;
    }
}
