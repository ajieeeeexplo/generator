<?php

namespace Ihero\Generator\Commands\Scaffold;

use Str;
use Illuminate\Console\GeneratorCommand;
use Ihero\Generator\Exceptions\RuntimeException;

class ServiceGeneratorCommand extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ih.make:service
                                {name : The name of the class.}
                                {--repo= : The name of repository.}
                                {--empty : Blank version of service.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new Service class';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Service';

    /**
     * Get the stub file for the generator.
     *
     * @param  string  $type
     * @return string
     */
    protected function getStub(string $type = '')
    {
        if ($type == 'repo') {
            return __DIR__.'/../../../stubs/service.repo.stub';
        }
        return __DIR__.'/../../../stubs/service.stub';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\Services';
    }

     /**
     * Build the class with the given name.
     *
     * @param  string  $name
     * @return string
     */
    protected function buildClass($name)
    {
        if ($this->option('empty')) {
            $stub = $this->files->get($this->getStub());

            return $this
                ->replaceNamespace($stub, $name)
                ->replaceClass($stub, $name);
        } else {
            $stub = $this->files->get($this->getStub('repo'));
            $replace = $this->buildRepoReplacements();

            return $this
                ->replaceNamespace($stub, $name)
                ->replaceRepoClass($stub, $replace)
                ->replaceClass($stub, $name);
        }
    }

    /**
     * Build the model replacement values.
     *
     * @return array
     */
    protected function buildRepoReplacements()
    {
        $repoClass = $this->parseReop($this->option('repo') ?? $this->getNameInput());

        if (! class_exists($repoClass)) {
            if ($this->confirm("A {$repoClass} repository does not exist. Do you want to generate it?", true)) {
                $this->call('ih.make:repository', ['name' => $repoClass]);
            }
        }

        return [
            'DummyFullRepositoryClass' => $repoClass,
            'DummyRepositoryClass' => class_basename($repoClass),
            'DummyRepositoryVariable' => lcfirst(class_basename($repoClass))
        ];
    }

    /**
     * Get the desired class name from the input.
     *
     * @return string
     */
    protected function getNameInput()
    {
        $name = trim($this->argument('name'));
        $name = Str::studly($name);

        return Str::is('*Service', $name) ?
            $name : Str::finish($name, 'Service');
    }

    /**
     * Get the fully-qualified model class name.
     *
     * @param  string  $model
     * @return string
     *
     * @throws RuntimeException
     */
    protected function parseReop($repository)
    {
        if (preg_match('([^A-Za-z0-9_/\\\\])', $repository)) {
            throw new RuntimeException('Repository name contains invalid characters.');
        }

        $repository = trim(str_replace('/', '\\', $repository), '\\');
        $repository = str_replace('Service', '', $repository);

        if (! Str::is('*Repository', $repository)) {
            $repository = Str::finish($repository, 'Repository');
        }
        if (Str::startsWith($repository, $rootNamespace = $this->laravel->getNamespace())) {
            $repository = class_basename($repository);
        }

        return sprintf('%sRepositories\\%s', $rootNamespace, $repository);
    }

    /**
     * Replace the model class for the given stub.
     *
     * @param  string  $stub
     * @param  array  $replace
     * @return $this
     */
    protected function replaceRepoClass(string &$stub, array $replace)
    {
        $stub = str_replace(
            array_keys($replace),
            array_values($replace),
            $stub
        );

        return $this;
    }
}
