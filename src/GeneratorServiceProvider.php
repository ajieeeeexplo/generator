<?php
namespace Ihero\Generator;

use Illuminate\Support\ServiceProvider;

class GeneratorServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     *
     * @return void
     */
    public function boot()
    {
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->commands('Ihero\Generator\Commands\Scaffold\ControllerGeneratorCommand');
        $this->commands('Ihero\Generator\Commands\Scaffold\ServiceGeneratorCommand');
        $this->commands('Ihero\Generator\Commands\Scaffold\RepositoryGeneratorCommand');
        $this->commands('Ihero\Generator\Commands\Scaffold\CriteriaGeneratorCommand');
        $this->commands('Ihero\Generator\Commands\Scaffold\ModelGeneratorCommand');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
